<!DOCTYPE html>
<html>
<head>
	<title>{{ $title }}</title>
</head>
<body>
	@component('componentes.children')
		@slot('title','Matemáticas')
		@slot('description','Libro matematicas')
	@endcomponent
	<br/>
	@component('componentes.children')
		@slot('title','Física')
		@slot('description','Libro fisica')
	@endcomponent
	<br/>
	@include('componentes.children',['title'=>'para componentes','description'=>'para componentes descripcion'])
</body>
</html>