<!DOCTYPE html>
<html>
<head>
	<title>@yield('title','No vino titulo')</title>
</head>
<body>
<div style="text-align: center;">
	<h1>@yield('modulo','No vino modulo')</h1>
</div>
<div style="text-align: center;">
	<h1>@yield('content')</h1>
</div>
<br/>
</body>
</html>