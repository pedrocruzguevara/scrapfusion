<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PruebaController extends Controller
{
    public function getSaludo(){
    	return "Hola Usuario, la hora es: ".date('H:i:s');
    }

    public function getUpper(Request $objRequest){
    	//dd($objRequest);
    	return strtoupper($objRequest->palabra);
    }

    public function getVista(){
    	//return view('practica.simple');
        /*
            $array=[1,2,3,4,5,6,7,8,9,0];
            return view('practica.parametros')->with(['title'=>'Es un título','description'=>'La descripción','array'=>$array,'flag'=>false]);
        */
        //return view('practica.importaciones')->with(['title'=>'Trabajando con componentes']);   

        return view('practica.heredero');
    }
}
