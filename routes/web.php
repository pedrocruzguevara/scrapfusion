<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Práctica de Rutas
Route::get('/saludo',function(){
	return "Hola Mundo";
});

Route::get('/mensaje/{nombre}/{apellido}/{id}',function($nombre,$apellido,$id){
	return "Los datos ingresado $nombre $apellido $id";
});

Route::get('/potencia/{base}/{exponente}',function($base, $exponente){
	return pow($base, $exponente);
});

Route::get('/potencia2/{base}/{exponente?}',function($base, $exponente=2){
	return pow($base, $exponente);
});

Route::get('/potencia3/{base?}/{exponente?}',function($base=5, $exponente=2){
	return pow($base, $exponente);
});

Route::get('/numero/{id}',function($id){
	return pow(3, $id);
})->where(['id'=>'[0-9]+']);

Route::get('/numero2/{id}/{id2}',function($id,$id2){
	return pow(3, $id);
})->where(['id'=>'[0-9]+','id2'=>'[5]+']);

Route::get('/slug/{accion}/{num}',function($accion,$num){
	switch ($accion) {
		case 'potencia':
			return pow(3, $num);
			break;
		
		case 'suma':
			return 4 + $num;
			break;
		
		default:
			# code...
			break;
	}
})->where(['num'=>'[0-9]+','accion'=>'potencia|suma']);


Route::get('/datos/{id}',function($id){
   return "número $id";
})->where(['id'=>'[0-9]+']);

Route::get('/datos/{id}',function($id){
   return "hola mundo";
})->where(['id'=>'hola']);

Route::get('/datos/{id}',function($id){
   return "hola tu parametro fue ".$id;
})->where(['id'=>'[-\w]+']);


Route::get('/horario',function(){
   return date('Y-m-d H:i:s');
})->name('hora');



Route::group(['prefix'=>'filtros'],function(){
	Route::get('/datos/{id}',function($id){
	   return "número $id";
	})->where(['id'=>'[0-9]+']);

	Route::get('/datos/{id}',function($id){
	   return "hola mundo";
	})->where(['id'=>'hola']);

	Route::get('/datos/{id}',function($id){
	   return "hola tu parametro fue ".$id;
	})->where(['id'=>'[-\w]+']);
});


Route::get('/saludo', 'PruebaController@getSaludo')->name('saludo');
Route::get('/mayuscula/{palabra}', 'PruebaController@getUpper')->name('mayuscula');
Route::get('/vista-simple', 'PruebaController@getVista')->name('vistasimple');

/*
	resource para todos los métodos
*/
//Route::resource('resource','ResourceController');

/*
	resource exclusivo
*/	
//Route::resource('resource','ResourceController',['only'=>'index']);

/*
	resource except
*/	
//Route::resource('resource','ResourceController',['except'=>'index']);

Route::group(['prefix'=>'admin','as'=>'admin'], function(){
	Route::resource('role','RoleController');
	Route::resource('category','CategoryController');
});

